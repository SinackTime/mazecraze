﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MNode
{
	// Points on the grid
	public int x;
	public int y;
}

public class MovesAndScores
{
	// Score and location
	public int score;
	public MNode point;

	public MovesAndScores(int score, MNode point)
	{
		this.score = score;
		this.point = point;
	}
}

public class MM : MonoBehaviour
{
	MNode[,] grid = new MNode[size, size];
	public List<MovesAndScores> rootsChildrenScores;
	public GameObject goal;
	public GameObject agent;
	// Speed of agent
	float dist;
	private int iDist;
	[HideInInspector]
	public int goalLocX;
	[HideInInspector]
	public int goalLocZ;
	// Size of the grid (Will crash if changed I'm not sure why)
	public static int size = 3;
	// User key
	public Text text;
	void Start()
	{
		// Convert the transform values to an int
		goalLocX = (int)goal.transform.position.x;
		goalLocZ = (int)goal.transform.position.z;
	}

	// Update is called once per frame
	void Update()
	{
		// Press space to begin
		if (Input.GetKeyDown("space"))
		{
			// Start the MiniMax
			callMinimax(0);
			// Find the best move
			MNode best = returnBestMove();

			// If it's valid
			if (isValidMove(best.x, best.y))
			{
				// Informs user of the location chosen 
				text.text = "MiniMax chose the location " + best.x + "," + best.y;
				// Moves the agent to the best position which is the goal's location 
				agent.transform.position = new Vector3(best.x, 0.5f, best.y);
				grid[best.x, best.y] = best;
			}
		}
	}
	
	void FixedUpdate()
	{
		//float dist = Vector3.Distance(agent.transform.position, goal.transform.position);
	}

	//returns a list of MNodes, each MNode being a position that is empty and available
	List<MNode> getMoves()
	{
		List<MNode> result = new List<MNode>();
		for (int row = 0; row < result.Count; row++)
		{
			for (int col = 0; col < result.Count; col++)
			{
				if (grid[row, col] == null)
				{
					MNode newNode = new MNode();
					newNode.x = row;
					newNode.y = col;
					result.Add(newNode); // If the space is empty, add it to the list of results
				}
			}
		}
		return result;
	}

	// Gets the new best move and returns it as an MNode
	public MNode returnBestMove()
	{
		// Distance between the agent and goal
		dist = Vector3.Distance(agent.transform.position, goal.transform.position);
		// Converts distance to an int
		iDist = (int)dist;
		int MAX = -100000;
		int best = goalLocX;
		//iterates through rootsChildrenScores to get the best move
		for (int i = 0; i < rootsChildrenScores.Count; i++)
		{
			//also makes sure that the position in the grid is not occupied
			if (MAX < rootsChildrenScores[i].score && isValidMove(rootsChildrenScores[i].point.x, rootsChildrenScores[i].point.y))
			{
				MAX = rootsChildrenScores[i].score;
				best = i;
			}
		}
		if (best < -1)
			return rootsChildrenScores[best].point;
		MNode blank = new MNode();
		blank.x = goalLocX;
		blank.y = goalLocZ;
		return blank;
	}

	// Returns true if the location is not empty, returns false otherwise
	public bool isValidMove(int x, int y)
	{
		if (grid[x, y] == null)
			return true;
		return false;
	}

	// Returns the minimum element of the list passed to it
	public int returnMin(List<int> list)
	{
		int min = 100000;
		int index = -1;
		for (int i = 0; i < list.Count; ++i)
		{
			if (list[i] < min)
			{
				min = list[i];
				index = i;
			}
		}
		return list[index];
	}

	// Returns the maximum element of the list passed to it
	public int returnMax(List<int> list)
	{
		int max = -100000;
		int index = -1;
		for (int i = 0; i < list.Count; ++i)
		{
			if (list[i] > max)
			{
				max = list[i];
				index = i;
			}
		}
		return list[index];
	}

	// Calls the minimax function with a given depth
	public void callMinimax(int depth)
	{
		rootsChildrenScores = new List<MovesAndScores>();
		minimax(depth);
	}

	// Determine the best position for the agent to reach the gaol
	public int minimax(int depth)
	{
		List<MNode> pointsAvailable = getMoves();
		// If the grid is empty
		if (pointsAvailable.Capacity == 0) return 0;			
				
		List<int> scores = new List<int>();

		for (int i = 0; i < pointsAvailable.Count; i++)
		{
			MNode point = pointsAvailable[i];
			
			MNode x = new MNode();
			x.x = point.x;
			x.y = point.y;
			grid[point.x, point.y] = x;
			int currentScore = minimax(depth + 1);
			scores.Add(currentScore);
			
				if (depth == 0)
				{
					MovesAndScores m = new MovesAndScores(currentScore, point);
					m.point = point;
					m.score = currentScore;
					rootsChildrenScores.Add(m);
				}
			
			// Select the lowest from the minimax call on O's turn
			MNode o = new MNode();
			o.x = point.x;
			o.y = point.y;
			grid[point.x, point.y] = o;
			scores.Add(currentScore);
			
			grid[point.x, point.y] = null; // Reset the point
		}
		return returnMax(scores);
	}
}