﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseHover : MonoBehaviour
{
	public Text text;
	void OnMouseEnter()
	{
		text.material.color = Color.white;
	}
	
	void OnMouseExit()
	{
		text.material.color = Color.cyan;
	}
}
