﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoloController : MonoBehaviour
{
	public GameObject agent;
	[HideInInspector]
	public static int score;
	// GameOver
	public Text text;
	// User key
	public Text keyText;
	// Start is called before the first frame update
	void Start()
	{
		score = 0;
		keyText.text = "Press space to begin.";
	}
	// Update is called once per frame
	void Update()
	{
		if (isGameOver())
		{
			// Shows the user the game has ended
			text.gameObject.SetActive(true);
			if (Input.anyKey)
			{ 
				// Loads the level selector
				SceneManager.LoadScene("LevelMenu");
			}
		}
	}

	// Game is over once the goal is reached
	public bool isGameOver()
	{
		if (score >= 1)
		{
			return true;
		}
		return false;
	}
}

