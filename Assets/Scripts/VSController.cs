﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VSController : MonoBehaviour
{
    public GameObject aStarAgent;
    public GameObject MiniMaxAgent;
    [HideInInspector]
    public static int scoreA;
    [HideInInspector]
    public static int scoreM;
	// GameOver text 
	public Text text;
	// User key
	public Text keyText;

	// Start is called before the first frame update
	void Start()
    {
        scoreA = 0;
        scoreM = 0;
		keyText.text = "Press space to begin.";
    }
    // Update is called once per frame
    void Update()
    {
		if (isGameOver())
		{
			// Shows the user the game has ended
			text.gameObject.SetActive(true);
			if (Input.anyKey)
			{
				// Loads the level selector
				SceneManager.LoadScene("LevelMenu");
			}
		}
	}

	// Game is over once one agent reaches the goal
	public bool isGameOver()
	{
		if (scoreA >= 1)
		{
			text.text = "AStar won!";
			return true;
		}
		if (scoreM >= 1)
		{
			text.text = "MiniMax won!";
			return true;
		}
		return false;
	}
}
