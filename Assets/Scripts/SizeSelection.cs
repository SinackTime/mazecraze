﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SizeSelection : MonoBehaviour
{
    // For buttons
    public Button isSmall, isLarge, isBack;
    public bool rTSize, rDSize, back;
    // If a button is clicked that scene is loaded

    public void OnMouseUpAsButton()
    {
        if (isSmall && rTSize)
        {
            SceneManager.LoadScene("AStar RT S");
        }
        else if (isLarge && rTSize)
        {
            SceneManager.LoadScene("AStar RT L");
        }
        else if (isSmall && rDSize)
        {
            SceneManager.LoadScene("AStar RD S");
        }
        else if (isLarge && rDSize)
        {
            SceneManager.LoadScene("AStar RD L");
        }
        else if (back)
        {
            SceneManager.LoadScene("LevelMenu");
        }
    }
}
