﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour 
{
    // Target the agent moves to
    public GameObject target;
    // Pathfinding object
    public GameObject obj;
    PathFinding2 pathFinding;
    // Speed for the agent
    public float speed = 5f;
    List<Unit2> path;

    void Start()
    {
        // Setup pathfinding
        pathFinding = obj.GetComponent<PathFinding2>();
    }

    void Update()
    {
        // If the path exists
        if ( pathFinding.pathWasFound )
        {
            // Move to the target
            StopCoroutine(Move());
            path = pathFinding.path;
            pathFinding.pathWasFound = false;
            // Stop moving
            StartCoroutine(Move());
        }
    }

    /*
    Moves the agent to the target
    */
    IEnumerator Move()
    {
        //print("Seeker started his moving !!!");
        Vector3 currentUnit = path[0].realPosition;
        int unitIndex = 0;
        while(true)
        {
            if( transform.position == currentUnit + new Vector3(0,0.25f,0) ) // +0.25f to y position for nice movement of seeker
            {
                unitIndex++;
                if (unitIndex >= path.Count)
                    yield break;
                currentUnit = path[unitIndex].realPosition;
            }
            // Move towards the target at a set speed
            transform.position = Vector3.MoveTowards(transform.position, currentUnit + new Vector3(0, 0.25f, 0), speed);
            yield return null;
        }
        
    }
}