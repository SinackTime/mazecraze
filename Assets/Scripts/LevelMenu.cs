﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour
{
    // For buttons
    public Button isRT, isRD, isQuit;
    private bool rT, rD, quit = false;
    // If a button is clicked that scene is loaded

    public void RTChoice()
    {
        rT = true;
    }

    public void RDChoice()
    {
        rD = true;
    }

    public void QChoice()
    {
        quit = true;
    }

    // If a button is clicked that scene is loaded
    public void OnMouseUpAsButton()
    {
        if (rT)
        {
            SceneManager.LoadScene("SizeSelectionRT");
        }
        if (rD)
        {
            SceneManager.LoadScene("SizeSelectionRD");
        }
        if (quit)
        {
            Application.Quit();
        }
    }
}
