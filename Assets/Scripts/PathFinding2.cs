﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine.UI;

public class PathFinding2 : MonoBehaviour
{
    // Paths for the agent
    public List<Unit2> path = null;
    public List<Unit2> openListUnit2 = null;
    // Agent, Target
    public GameObject seeker, target;
    Gridd2 grid;
    // Time
    private Stopwatch time;
    public bool pathWasFound;
    public Text timeText, pathText;
    public Transform[] spawnPoints;
    private int rand;

    void Awake()
    {
        grid = GetComponent<Gridd2>();
    }

    void Start()
    {
        goalLocation();
    }
    
    void Update()
    {
        // Starts the game
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Finds the target
            FindPath(seeker.transform.position, target.transform.position);
            // Print the time and path length
            print(" *** time = " + time.ElapsedMilliseconds + " ms ***");
            print(" *** PathLength = " + path[path.Count-1].gCost + " ***");
            timeText.text = "AStar time " + time.ElapsedMilliseconds.ToString() + "ms";
            pathText.text = "AStar path length " + path[path.Count - 1].gCost.ToString();
            pathWasFound = true;
        }
    }

    /*
    Finds the path to the goal
    */
    public void FindPath(Vector3 seekerPos, Vector3 targetPos)
    {
        time = new Stopwatch();
        // Starts the clock
        time.Start();
        Unit2 seekerUnit2 = grid.fromRealPosToUnit2(seekerPos);
        Unit2 targetUnit2 = grid.fromRealPosToUnit2(targetPos);

        openListUnit2 = new List<Unit2>(); // for computing Unit2 of openList ( need to draw path )
        List<Unit2> openList = new List<Unit2>();
        List<Unit2> closedList = new List<Unit2>();
        // Add the agent 
        openList.Add(seekerUnit2);
        openListUnit2.Add(seekerUnit2); //...

        while (openList.Count > 0)
        {
            Unit2 currentUnit2 = openList[0];
            // Cost calculation
            for (int i = 1; i < openList.Count; i++)
            {
                if ((openList[i].fCost < currentUnit2.fCost) || (openList[i].fCost == currentUnit2.fCost && openList[i].hCost < currentUnit2.hCost))
                {
                    currentUnit2 = openList[i];
                }
            }

            // Adds or removes according to the cost
            openList.Remove(currentUnit2);
            closedList.Add(currentUnit2);
            
            // End of path
            if (currentUnit2.realPosition == targetUnit2.realPosition)
            {
                time.Stop();
                ComputePath(seekerUnit2, targetUnit2);
                return;
            }

            // Compute path with the lowest cost
            foreach (Unit2 neighbour in grid.GetNeighbours(currentUnit2))
            {
                if (!neighbour.walkable || closedList.Contains(neighbour))
                {
                    continue;
                }

                int pathToNeighbour = currentUnit2.gCost + GetDistance(currentUnit2, neighbour);
                if (pathToNeighbour < neighbour.gCost || !openList.Contains(neighbour))
                {
                    neighbour.gCost = pathToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetUnit2);
                    neighbour.parent = currentUnit2;
                    openList.Add(neighbour);
                    openListUnit2.Add(neighbour); // ...
                }
            }
        }
    }
    
    /*
    Calcuates the path
    */
    void ComputePath(Unit2 startUnit2, Unit2 endUnit2)
    {
        Unit2 currentUnit2 = endUnit2;
        List<Unit2> newPath = new List<Unit2>(); // for computing shotest path
        while (currentUnit2.realPosition != startUnit2.realPosition)
        {
            newPath.Add(currentUnit2);
            currentUnit2 = currentUnit2.parent;
        }
        newPath.Reverse();
        path = newPath;
    }

    /*
    Returns the distance of the agent's path 
    */
    int GetDistance(Unit2 Unit21, Unit2 Unit22)
    {
        int dstX = Mathf.Abs(Unit21.x - Unit22.x);
        int dstY = Mathf.Abs(Unit21.y - Unit22.y);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }

    void goalLocation()
    {
        rand = Random.Range(0, 3);
        switch(rand)
        {
            case 0:
                target.transform.position = spawnPoints[rand].position;
                break;
            case 1:
                target.transform.position = spawnPoints[rand].position;
                break;
            case 2:
                target.transform.position = spawnPoints[rand].position;
                break;
            case 3:
                target.transform.position = spawnPoints[rand].position;
                break;
            default:
                target.transform.position = new Vector3(0, 0, 0);
                break;
        }
    }
}
