﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "AStar")
        {
            // Add a point to AStar
            VSController.scoreA++;
            SoloController.score++;
            Destroy(gameObject);
        }
        else if(other.tag == "MiniMax")
        {
            // Add a point to MiniMax
            VSController.scoreM++;
            SoloController.score++;
            Destroy(gameObject);
        }
    }
}
